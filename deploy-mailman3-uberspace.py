import time
import os
import deploymentutils as du
from os.path import join as pjoin

from ipydex import IPS, activate_ips_on_exception

# simplify debugging
activate_ips_on_exception()

# this script assumes that your ssh-key is authorized on the remote machine
# (https://manual.uberspace.de/basics-ssh/#ssh-working-with-keys):
# call this before running the script:
# eval $(ssh-agent); ssh-add -t 10m


# This script is based on: https://lab.uberspace.de/guide_mailman-3.html

# this has to be done manually: `uberspace mail user add forwarder`
# the password has to match `cf("smtp_pass")`


# -------------------------- Begin Essential Config section  ------------------------

# load configuration:

cf = du.get_nearest_config("mailman-config-production.ini")

remote = cf("remote_hostname")
user = cf("user")

asset_dir = pjoin(du.get_dir_of_this_file(), "files")  # contains the templates
temp_workdir = pjoin(du.get_dir_of_this_file(), "tmp_workdir")  # this will be deleted/overwritten

# -------------------------- Begin Optional Config section -------------------------
# if you know what you are doing you can adapt these settings to your needs

# this is the root dir of the project (where setup.py lies)
# if you maintain more than one instance (and deploy.py lives outside the project dir, this has to change)
project_src_path = os.path.dirname(du.get_dir_of_this_file())

# directory for deployment files (e.g. database files)
deployment_dir = cf("deployment_dir")

# name of the directory for the virtual environment:
venv = cf("venv")
venv_path = f"/home/{user}/venvs/{venv}"

# because uberspace offers many pip_commands:
pipc = "pip3.8"

du.argparser.add_argument("--dbg", action="store_true",
                          help="start interactive shell for debugging. Then exit")

args = du.parse_args()

final_msg = f"Deployment script {du.bgreen('done')}."

if not args.target == "remote":
    raise NotImplementedError

# this is where the code will live after deployment
target_deployment_path = f"/home/{user}/{deployment_dir}"
static_root_dir = f"{target_deployment_path}/collected_static"
debug_mode = False

# TODO: make a backup of the remote-data
# print a warning for data destruction
du.warn_user(cf("PROJECT_NAME"), args.target, args.unsafe, deployment_path=target_deployment_path,
             user=user, host=remote)

# ensure clean workdir
os.system(f"rm -rf {temp_workdir}")
os.makedirs(temp_workdir)

c = du.StateConnection(remote, user=user, target=args.target)

if args.dbg:
    # c.activate_venv(f"{venv_path}/bin/activate")

    IPS()
    exit()

if args.initial and 0:

    c.run(f"{pipc} install --user virtualenv")

    print("create and activate a virtual environment inside $HOME")
    c.run(f"rm -rf {venv_path}")
    c.run(f"mkdir -p {venv_path}")
    c.chdir(f"/home/{user}/venvs/")

    c.run(f"virtualenv -p python3.8 {venv}")
    c.activate_venv(f"{venv_path}/bin/activate")

    # upgrading pip and setuptools seems to be necessary to prevent errors on uberspace
    c.run(f"pip install --upgrade pip")
    c.run(f"pip install --upgrade setuptools")

    # ensure that the same version of deploymentutils like on the controller-pc is also in the server
    c.deploy_this_package()

    c.run(f"pip install --upgrade mailman hyperkitty postorius mailman-hyperkitty whoosh uwsgi")

    # ↓↓↓ -- https://lab.uberspace.de/guide_mailman-3.html#get-dart-sass --
    c.chdir(f"/home/{user}/tmp")

    dart_version = "1.35.2"
    c.run(
        f"wget https://github.com/sass/dart-sass/releases/download/{dart_version}/dart-sass-{dart_version}-linux-x64.tar.gz")
    c.run(f"tar xzvf dart-sass-{dart_version}-linux-x64.tar.gz dart-sass")
    c.run(f"rm -rf ~/bin/dart-sass")
    c.run(f"mv  dart-sass ~/bin/")
    c.run(f"rm dart-sass-{dart_version}-linux-x64.tar.gz")

    # ↓↓↓ -- https://lab.uberspace.de/guide_mailman-3.html#get-the-mailman-suite-example-configuration --
    c.run(f"git clone https://gitlab.com/mailman/mailman-suite.git")
    c.run(f"mv -f mailman-suite/mailman-suite_project/ .")
    c.run(f"rm -rf mailman-suite")
    c.run(f"rm -rf {cf('MAILMAN_DEPLOYMENT_PATH')}")
    c.run(f"mv -f mailman-suite_project/ {cf('MAILMAN_DEPLOYMENT_PATH')}")

    #
    # ## generate and upload necessary django uwsgi infrastructure and other config files  ###
    #

    c.activate_venv(f"{venv_path}/bin/activate")

    # generate the general uwsgi ini-file

    tmpl_dir = os.path.join("uberspace", "etc", "services.d")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "template_uwsgi.ini"),
        target_path=pjoin(temp_workdir, tmpl_dir, "uwsgi.ini"),
        context=dict(venv_abs_bin_path=f"{venv_path}/bin/")
    )

    c.run(f"mkdir -p ~/uwsgi/apps-enabled", target_spec="remote")
    c.run(f"touch ~/uwsgi/err.log", target_spec="remote")
    c.run(f"touch ~/uwsgi/out.log", target_spec="remote")

    # https://lab.uberspace.de/guide_mailman-3.html#get-qmail-helper-scripts
    c.chdir("~/bin", target_spec="remote")
    c.run("wget https://gitlab.com/mailman/mailman/raw/master/contrib/qmail-lmtp", target_spec="remote")
    c.run("chmod +x qmail-lmtp", target_spec="remote")

    # generate config file for django uwsgi-app
    tmpl_dir = pjoin("uberspace", "uwsgi", "apps-enabled")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "template_mailman-suite.ini"),
        target_path=pjoin(temp_workdir, tmpl_dir, "mailman-suite.ini"),
        context=dict(venv_abs_bin_path=f"{venv_path}/bin/",
                     MAILMAN_DEPLOYMENT_PATH=cf("MAILMAN_DEPLOYMENT_PATH"))
    )

    # https://lab.uberspace.de/guide_mailman-3.html#configure-mailman-core
    tmpl_dir = pjoin("uberspace", "var", "etc")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "template_mailman.cfg"),
        target_path=pjoin(temp_workdir, tmpl_dir, "mailman.cfg"),
        context=dict(user=user, smtp_user=cf("smtp_user"), smtp_host=cf("smtp_host"),
                     smtp_pass=cf("smtp_pass"), restadmin=cf("admin_user"), restpass=cf("admin_pass"),
                     venv=cf("venv"))
    )

    # https://lab.uberspace.de/guide_mailman-3.html?highlight=mailman#daemonizing-mailman-core
    tmpl_dir = pjoin("uberspace", "etc", "services.d")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "template_mailman3.ini"),
        target_path=pjoin(temp_workdir, tmpl_dir, "mailman3.ini"),
        context=dict(venv=cf("venv"))
    )
    # the next file can be used unchanged (but are handled as templates anyway)

    # https://lab.uberspace.de/guide_mailman-3.html?highlight=mailman#configure-hyperkitty
    tmpl_dir = pjoin("uberspace", "var", "etc")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "mailman-hyperkitty.cfg"),
        target_path=pjoin(temp_workdir, tmpl_dir, "mailman-hyperkitty.cfg"),
        context=dict(hk_api_key=cf("hk_api_key"))
    )

    # create logfiles for mailman daemon
    c.run(f"mkdir -p ~/var/logs", target_spec="remote")
    c.run(f"touch ~/var/logs/daemon_err.log", target_spec="remote")
    c.run(f"touch ~/var/logs/daemon_out.log", target_spec="remote")

    # https://lab.uberspace.de/guide_mailman-3.html?highlight=mailman#adjusting-django-configuration
    tmpl_dir = pjoin("uberspace", "mailman_deployment")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "settings_local.py"),
        target_path=pjoin(temp_workdir, tmpl_dir, "settings_local.py"),
        context=dict()
    )

    # note: there is already a section in mailmans settings.py such that the above file is automatically imported if present

    tmpl_dir = pjoin("uberspace", "mailman_deployment")
    du.render_template(
        tmpl_path=pjoin(asset_dir, tmpl_dir, "post_install_script.py"),
        target_path=pjoin(temp_workdir, tmpl_dir, "post_install_script.py"),
        context=dict()
    )

    #
    # ## upload config files to remote $HOME ##
    #
    srcpath1 = os.path.join(temp_workdir, "uberspace")
    filters = "--exclude='**/README.md' --exclude='**/template_*'"  # not necessary but harmless
    c.rsync_upload(srcpath1 + "/", "~", filters=filters, target_spec="remote")

    # upload the config file
    c.rsync_upload(cf.path, cf("MAILMAN_DEPLOYMENT_PATH"), target_spec="remote")

    #
    # https://lab.uberspace.de/guide_mailman-3.html?highlight=mailman#setting-up-django
    #
    c.activate_venv(f"{venv_path}/bin/activate")
    c.chdir(cf("MAILMAN_DEPLOYMENT_PATH"))

    c.run("python3 manage.py flush --no-input", target_spec="remote")
    c.run("python3 manage.py migrate", target_spec="remote")
    c.run("python3 manage.py collectstatic --no-input", target_spec="remote")
    c.run(f'DJANGO_SUPERUSER_PASSWORD={cf("DJANGO_ADMIN_PASS")} python manage.py createsuperuser'
          f' --username {user} --email {user}@uber.space --no-input', target_spec="both"
          )

    #
    # rename example site by adapting the database
    #

    # get the filname of the config file (and pass it as command line arg in the next line)
    cf_fname = os.path.split(cf.path)[1]

    c.run(f'python3 post_install_script.py {cf_fname}', target_spec="remote")

    #
    # make site reachable
    #

    domains = cf("US_WEB_BACKEND_DOMAINS", cast=cf.Csv())

    res = c.run("uberspace web domain list")
    configured_domains = res.stdout.split()

    if domains:
        print(f"\nThe following output might be {du.bgreen('important')}:\n")
    for domain in domains:
        domain = domain.strip("/")
        if domain in configured_domains:
            # ensure idempotence of this script
            c.run(f'uberspace web domain del {domain}', target_spec="remote")

        c.run(f'uberspace web domain add {domain}', target_spec="remote")
        c.run(f'uberspace web backend set {domain}/ --http --port 8000', target_spec="remote")
        c.run(f'uberspace web backend set {domain}/static --apache', target_spec="remote")

    # restart uwsgi to apply changes
    c.run('supervisorctl reread', target_spec="remote")
    c.run('supervisorctl update', target_spec="remote")
    c.run('supervisorctl restart uwsgi', target_spec="remote")
    print("waiting for uwsgi to start")
    time.sleep(5)

    res = c.run('supervisorctl status', target_spec="remote")

    assert "uwsgi" in res.stdout
    assert "RUNNING" in res.stdout


if args.initial and 1:
    c.activate_venv(f"{venv_path}/bin/activate")
    #
    # configure qmail
    #
    config_line = f"|/home/{user}/bin/qmail-lmtp 8024 1 {user}.local.uberspace.de"
    res = c.run("cat ~/.qmail-default")
    time_str = time.strftime("%Y-%m-%d__%H-%M-%S")
    if config_line not in res.stdout:
        c.run(f'cp  ~/.qmail-default ~/._qmail-default-backup-{time_str}')
        c.run(f'echo "{config_line}" > ~/.qmail-default')
    else:
        print("config line was already present in ~/.qmail-default")

    #
    # write a new line to crontab for recurring tasks
    #
    # it is possible that the users crontab does not exist yet. Then `crontab` fails (nonzero exit code)
    # -> this is handled
    cron_line = f'@daily /home/{cf("user")}/venvs/{cf("venv")}/bin/mailman digests --send # MAILMAN_DIGEST'
    res = c.run("crontab -l", warn=False)
    if res.exited != 0 or "mailman digests" not in res.stdout:
        c.run(f'(crontab -l 2>/dev/null; echo "{cron_line}") | crontab -')
        print("-> new crontab line written\n")
    elif "mailman digests" in res.stdout and cron_line not in res.stdout:
        # this e.g. happens if venv changes
        print("\n", du.yellow("Threre is a problem with your crontab:"))
        msg = "`crontab -l` already contains a line for mailman digest, but it is not as expected.\n" \
              "Maybe your configuration for `venv` changed since last deployment?\n\n" \
              f"Current crontab:\n{res.stdout}\n\n Expected line:\n{cron_line}\n\n" \
              "-> This must be solved manually via `crontab -e`\n\n"
        print(msg)
    else:
        print("crontab line was already present\n")

print(final_msg)
