# uberspace-autodeploy-mailman3

This script is an automated version of the following guide: <https://lab.uberspace.de/guide_mailman-3.html>.


This repo contains an experimental script (+ auxiliary data) that is an automated version of the mailman3 installation guide: <https://lab.uberspace.de/guide_mailman-3.html>. The goal is to have a working mailman3 installation in < 10min without using specialized tools like ansible. See also the [Motivation section](#motivation) below.

As of now it is tested only rudimentary. **Use at your own risk**.


The main components are:

- `deploy-mailman3-uberspace.py` – a script which does the most steps from [the guide](https://lab.uberspace.de/guide_mailman-3.html)
- `mainman-config-*.ini` a configuration file which is *one compact place* for all instance-specific data.
- A bunch of additional configuration files which are oftend stored as templates to insert instance-specific data from the config.

# Usage


## Steps on your uberspace machine (connected via ssh):

- Create a mail user (use the `smtp_pass` from your config as password)
```
[isabell@stardust ~]$ uberspace mail user add forwarder
```

- Setup the mail domains (see [uberspace docs](https://manual.uberspace.de/mail-domains/#mail-domains) for details) e.g.
    - For mailman3:
    ```
    uberspace mail domain add lists.your-domain.org
    ```
    - For general mails (e.g. contact@):
    ```
    uberspace mail domain add your-domain.org
    ```

## Steps on your local machine:

- Clone the repo
- Copy `mailman-config-example.ini` to `mailman-config-production.ini` and adapt that file.
- Open a python3 environment where you can install python packages, e.g. with virtualenv or conda.
- Run `pip install -r requirements.txt`
- Run `python3 deploy-mailman3-uberspace.py --initial remote`
    - If you use custom domains in your , there shoud be at least one output section containing something like
    ```
    Now you can use the following records for your DNS:
    A -> 95.143.123.456
    AAAA -> ee35:21fe:dd50:2420:da4c:12f2:4683:a939
    ```
    - Use this information to configure your DNS records (probably via the web interface of your domain provider)

# TODO

- Include backup code
- More tests on fresh uberspace accounts
    - Especially domain handling
- Test and adaptions for windows and mac.


# Motivation

Uberspace is a great concept for small and medium-sized projects: Almost the flexibility of a virtual machine, but without the hazzle of admin work (security, certificates, ...). And all this for a great and fair self-chosen price.

A large part of the concept relies on user-generated how-to guides (["Uberspace lab"](https://lab.uberspace.de/)). These guides are rendered HTML documents with code sections and explanations. This is great for learning, however not so great for actual deploying because the steps have to be copy-pasted and adapted individually.

This makes it tedious to redeploy a service, and contributes to a "never touch a running system" attitude.

The goal of this repo is to automate the deployment of mailman3 in an idempotent way (running the script multiple times does not harm). This allows administrators easily manage the deployment configuration for each instance, as it would be the case with a professional configuration management system such as *Ansible*. However, almost no addional tool-knowledge is required as the deployment script merely is a collection of commands (from the respective how-to guide) but with some values taken from a configuration file.

This should simplify and professionalize the deployment process and thus make it accessible for more people. Running self-hosted webservices contributes to digital autonomy and sovereignty.
