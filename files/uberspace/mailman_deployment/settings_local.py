# this is based on https://lab.uberspace.de/guide_mailman-3.html?highlight=mailman#adjusting-django-configuration

import deploymentutils as du
cf = du.get_nearest_config("mailman-config-production.ini")


BASE_DIR = f'/home/{cf("user")}/var/'

SECRET_KEY = cf("SECRET_KEY")

DEBUG = cf("DEBUG", cast=bool)

ADMINS = (
     (cf("ADMIN_NAME"), cf("ADMIN_MAIL")),
)

ALLOWED_HOSTS = cf("ALLOWED_HOSTS", cast=cf.Csv())

MAILMAN_REST_API_URL = f'http://{cf("user")}.local.uberspace.de:8001'
MAILMAN_REST_API_USER = cf("admin_user")
MAILMAN_REST_API_PASS = cf("admin_pass")
MAILMAN_ARCHIVER_KEY = cf("hk_api_key")
MAILMAN_ARCHIVER_FROM = ('127.0.0.1', '::1')


# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')  # Uncomment
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_SCHEME', 'https')  # Uncomment

STATIC_ROOT = f'/home/{cf("user")}/html/static/'

DEFAULT_FROM_EMAIL = f'forwarder@{cf("user")}.uber.space'

SERVER_EMAIL = f'{cf("user")}@uber.space'

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = cf("remote_hostname")
EMAIL_PORT = 587
EMAIL_USE_TLS = True
# Use previously created mail user/password
EMAIL_HOST_USER = cf("smtp_user")
EMAIL_HOST_PASSWORD = cf("smtp_pass")


COMPRESS_PRECOMPILERS = (
   ('text/less', 'lessc {infile} {outfile}'),
   ('text/x-scss', f'/home/{cf("user")}/bin/dart-sass/sass' + ' {infile} {outfile}'),
   ('text/x-sass', f'/home/{cf("user")}/bin/dart-sass/sass' + ' {infile} {outfile}'),
)


SOCIALACCOUNT_PROVIDERS = {}

# Comment the following lines out to test sending mail
# if DEBUG:
#     EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
#     EMAIL_FILE_PATH = os.path.join(BASE_DIR, 'emails')
