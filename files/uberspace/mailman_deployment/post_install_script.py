# see https://lab.uberspace.de/guide_mailman-3.html#setting-up-django

import os
import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import django
django.setup()

from django.contrib.sites.models import Site
import deploymentutils as du


# get name of config file from command line:
cf_fname = sys.argv[1]

# assume this file is located next to settings.py
cf = du.get_nearest_config(cf_fname)

print("Changing SITE_NAME in database ...")
site = Site.objects.get(name='example.com')
site.name = cf("SITE_NAME")
site.domain = cf("SITE_DOMAIN")
site.save()

print("done")
